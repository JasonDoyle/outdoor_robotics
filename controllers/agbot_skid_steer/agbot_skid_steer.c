/*
 * File:          agbot_skid_steer.c
 * Date:
 * Description:
 * Author:
 * Modifications:
 */

/*
 * You may need to add include files like <webots/distance_sensor.h> or
 * <webots/motor.h>, etc.
 */
#include <stdio.h>
#include <stdlib.h>

#include <webots/distance_sensor.h>
#include <webots/motor.h>
#include <webots/robot.h>
#include <webots/gps.h>
#include <webots/compass.h>

#include <webots/range_finder.h>
#include <webots/camera.h>

#define TIME_STEP 64
#define WHEEL_RADIUS 0.127
#define WHEEL_DISTANCE 0.3


enum states{start, pause, turn, drive};
enum states current_state = start;

double get_bearing_in_degrees(WbDeviceTag compass);
double bearing_to_waypoint_degrees(const double current_pos[], double waypoint[]);
//void drive_to_waypoint();



int main(int argc, char **argv) {

  double angle_to_waypoint = 0;
  double current_heading = 0;
  double new_heading = 0;
  double waypoint[5][2] = { {1,2}, {3,2}, {5,0}, {-3,-4},{0,0}};
  
  int j = 0;
  
  
  wb_robot_init();
  int i;
  bool avoid_obstacle_counter = 0;
  WbDeviceTag ds[2];
  char ds_names[2][10] = {"ds_left", "ds_right"};
  for (i = 0; i < 2; i++) {
    ds[i] = wb_robot_get_device(ds_names[i]);
    wb_distance_sensor_enable(ds[i], TIME_STEP);
  }
  WbDeviceTag wheels[4];
  char wheels_names[4][8] = {"wheel1", "wheel2", "wheel3", "wheel4"};
  for (i = 0; i < 4; i++) {
    wheels[i] = wb_robot_get_device(wheels_names[i]);
    wb_motor_set_position(wheels[i], INFINITY);
  }
  
  WbDeviceTag range = wb_robot_get_device("kinect range");
  wb_range_finder_enable(range, TIME_STEP);
  
  WbDeviceTag camera = wb_robot_get_device("kinect color");
  wb_camera_enable(camera, TIME_STEP);
  
    /* get and enable GPS device */
  WbDeviceTag gps = wb_robot_get_device("gps");
  wb_gps_enable(gps, TIME_STEP);
  
  WbDeviceTag compass = wb_robot_get_device("compass");
  wb_compass_enable(compass, TIME_STEP);
  
  

  
  
  double left_speed = 0;
  double right_speed = 0;
  
  double omega = 2;
  double vel = 1;
    
  while (wb_robot_step(TIME_STEP) != -1) {
     /* get coordinates and speed */
    const double *position = wb_gps_get_values(gps);
    const char *latitude = wb_gps_convert_to_degrees_minutes_seconds(position[0]);
    const double altitude = position[2];
    const char *longitude = wb_gps_convert_to_degrees_minutes_seconds(position[1]);
    const double speed = wb_gps_get_speed(gps);
    
    printf("Latitude is: %lfdeg / %s\n", position[0], latitude);
    printf("Longitude is: %lfdeg / %s\n", position[1], longitude);
    printf("Altitude is: %lf [m]\n", altitude);
    printf("Speed is: %lf [m/s]\n", speed);
    printf("\n");

    /* those char * returned by 'wb_gps_convert_to_degrees_minutes_seconds' need to be freed */
    free((void *)latitude);
    free((void *)longitude);
    
    const double *direction = wb_compass_get_values(compass);
    
    current_heading =  get_bearing_in_degrees(compass);
    
    printf("Compass is X: %lf Y: %lf Z: %lf\n", direction[0], direction[1], direction[2]);
    printf("Compass bearing: %lf\n", round(current_heading));
     
    
    
    


    switch(current_state) {
      case start:
        printf("Current state: start \n");
        omega = 0;
        vel = 0;
        // new_heading = current_heading - angle_to_waypoint;
        current_state = turn;
        break;
      case pause:
        printf("Current state: pause \n");
        omega = 0;
        vel = 0;
        break;
      case turn:
        printf("Current state: turn \n");
        angle_to_waypoint = bearing_to_waypoint_degrees(position, waypoint[j]);
        printf("Angle to waypoint: %lf\n", angle_to_waypoint);
        // new_heading = current_heading - angle_to_waypoint;
        if(angle_to_waypoint > 0) {
          new_heading = 360 - angle_to_waypoint;
        }
        else if(angle_to_waypoint < 0) {
          new_heading = 360 + angle_to_waypoint;
        }
        printf("New heading bearing: %lf\n", round(new_heading));
        
        if(round(current_heading) > round(new_heading)) {
          omega = 2;
          vel = 0;
        }
        else if(round(current_heading) < round(new_heading)) {
          omega = -2;
          vel = 0;
        }
        else if(round(current_heading) == round(new_heading)) {
          omega = 0;
          vel = 0;
          current_state = drive;
        }
        break;
      case drive:
        printf("Current state: drive \n");
        omega = 0;
        vel = 1;
        if(j==5) {
          current_state = pause;
        }
        else if(round(position[0]*10)/10 == round(waypoint[j][0])) {
          j++;
          current_state = turn;
        }
        break;
      default:
        printf("Current state: default \n");
        break;
    }
    
    // omega = angle_to_waypoint/10;
    
    left_speed = (2*vel-omega*WHEEL_DISTANCE)/(2*WHEEL_RADIUS);
    right_speed = (2*vel+omega*WHEEL_DISTANCE)/(2*WHEEL_RADIUS);
    
    printf("Left_speed: %lf\n", left_speed);
    printf("Right_speed: %lf\n", right_speed);
  
    if (avoid_obstacle_counter > 0) {
      avoid_obstacle_counter--;
      left_speed = 1.0;
      right_speed = -1.0;
    } else { // read sensors
      double ds_values[2];
      for (i = 0; i < 2; i++)
        ds_values[i] = wb_distance_sensor_get_value(ds[i]);
      if (ds_values[0] < 950.0 || ds_values[1] < 950.0)
        avoid_obstacle_counter = 100;
    }
    wb_motor_set_velocity(wheels[0], left_speed);
    wb_motor_set_velocity(wheels[1], right_speed);
    wb_motor_set_velocity(wheels[2], left_speed);
    wb_motor_set_velocity(wheels[3], right_speed);
  }
  wb_robot_cleanup();
  return 0;  // EXIT_SUCCESS
}


double get_bearing_in_degrees(WbDeviceTag compass) {
  const double *north = wb_compass_get_values(compass);
  double rad = atan2(north[1], north[0]);
  double bearing = (rad - 1.5708) / M_PI * 180.0;
  if (bearing < 0.0)
    bearing = bearing + 360.0;
  return bearing;
}


double bearing_to_waypoint_degrees(const double current_pos[], double waypoint[]) {
  double delta_x = waypoint[0] - current_pos[0];
  double delta_y = waypoint[1] - current_pos[1];
  
  double bearing_rad = atan2(delta_y, delta_x);
  double bearing_deg = 180*bearing_rad/M_PI;
  
  return bearing_deg;
}


//void drive_to_waypoint(double current_pos[], double waypoint[]) {
  //double current_bearing = 
  /*double current_bearing = get_bearing_in_degrees(WbDeviceTag compass)*/
//  printf("Bearing to waypoint: %lf\n", &position);
//}